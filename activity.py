class Camper:
    def __init__(self, name: str, batch: int, course_type: str) -> None:
        self.name: str = name
        self.batch: int = batch
        self.course_type: str = course_type

    def career_track(self) -> None:
        print(f"Currently enrolled in the {self.course_type} program.")
    
    def info(self)  -> None:
        print(f"My name is {self.name} of batch {self.batch}")


zuitt_camper: Camper = Camper("Horhe", 276, "python short course")

print(f"Camper Name: {zuitt_camper.name}")
print(f"Camper Batch: {zuitt_camper.batch}")
print(f"Camper Course: {zuitt_camper.course_type}")

zuitt_camper.info()
zuitt_camper.career_track()


